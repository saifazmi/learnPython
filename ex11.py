print "How old are you?",
age = raw_input()
print "How tall are you?",
height = raw_input()
print "How much do you weigh?",
weight = raw_input()

print "So, you're {!r} old, {!r} tall and {!r} heavy.".format(
    age, height, weight)

# We put a , (comma) at the end of each print line.
# This is so print doesn't end the line with a newline character and go to
# the next line.

# Study Drill 1 -
# The raw_input function reads a line from input coverts it into a string
# strips any trailing newline and returns that.
# We can provide a prompt as argument for this function.
# The prompt will be written to standard output wihtout a trailing newline.

# Study Drill 2 -
name = raw_input("What is your name again? ")
print "Ah now I remember you are {!r}".format(name)
