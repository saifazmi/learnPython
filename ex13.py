from sys import argv

script, first, second, third = argv

print "The script is called:", script
print "Your first variable is:", first
print "Your second variable is:", second
print "Your third variable is:", third

# Study Drill 3 -
# ----------------
fourth = raw_input("Enter a fourth value: ")
print "Your fourth variable is: ", fourth
# NOTES:
# The argv is the "argument variabl"
# This variable holds the arguments you pass to your Python script when
# you run it.

# Line 3 "unpacks" argv so that, rather than holding all the arguments, it
# gets assigned to four variables

# Study Drill 1:
# --------------
# Error msg when not passing in enough parameters -
# Traceback (most recent call last):
#   File "ex13.py", line 3, in <module>
#     script, first, second, third = argv
# ValueError: need more than 1 value to unpack
#
# When passing more than required arguments -
# Traceback (most recent call last):
#   File "ex13.py", line 41, in <module>
#     script, first, second= argv
# ValueError: too many values to unpack

# Study Drill 2:
# --------------
# Script with more arguments -

# script, first, second, third, fourth, fifth = argv

# print "The script is called:", script
# print "Your first variable is:", first
# print "Your second variable is:", second
# print "Your third variable is:", third
# print "Your fourth variable is:", fourth
# print "Your fifth variable is:", fifth

# Script with less arguments -

# script, first, second= argv

# print "The script is called:", script
# print "Your first variable is:", first
# print "Your second variable is:", second
