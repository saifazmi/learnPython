# Prints a string
print "Mary had a little lamb."
# Prints a string with a format character and assigns it a value
print "Its fleece was white as %s." % 'snow'
# Prints a string
print "And everywhere that Mary went."
# Prints a dot x10 times
print "." * 10  # what'dd that do?

# Assigns value 'C' to variable 'end1'
end1 = "C"
# Assigns value 'h' to variable 'end2'
end2 = "h"
# Assigns value 'e' to variable 'end3'
end3 = "e"
# Assigns value 'e' to variable 'end4'
end4 = "e"
# Assigns value 's' to variable 'end5'
end5 = "s"
# Assigns value 'e' to variable 'end6'
end6 = "e"
# Assigns value 'B' to variable 'end7'
end7 = "B"
# Asisgns value 'u' to variable 'end8'
end8 = "u"
# Assigns value 'r' to variable 'end9'
end9 = "r"
# Assigns value 'g' to variable 'end10'
end10 = "g"
# Assigns value 'e' to variable 'end11'
end11 = "e"
# Assigns value 'r' to variable 'end12'
end12 = "r"

# watch that comma at the end. try removing it to see what happens
# Prints the values stored in variable end1 to end6
print end1 + end2 + end3 + end4 + end5 + end6,
# Prints the value stored in variable end7 + end12
print end7 + end8 + end9 + end10 + end11 + end12

# With comma -
# Mary had a little lamb.
# Its fleece was white as snow.
# And everywhere that Mary went.
# ..........
# Cheese Burger
#
# Without comma -
# Mary had a little lamb.
# Its fleece was white as snow.
# And everywhere that Mary went.
# ..........
# Cheese
# Burger
