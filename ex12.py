age = raw_input("How old are you? ")
height = raw_input("How tall are you? ")
weight = raw_input("How much do you weigh? ")

print "So, you're {!r} old, {!r} tall and {!r} heavy.".format(
    age, height, weight)

# Study Drill 3 -
# The pydoc module automatically generates documentation from Python modules.
# The documentation can be presented as pages of text on the console,
# served to a Web browser, or saved to HTML files.
