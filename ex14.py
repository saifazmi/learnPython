from sys import argv

# Study Drill 3 -
# ----------------
script, user_name, user_id = argv

# Study Drill 2 -
# ----------------
# prompt = '> '
prompt = '=> '

print "Hi %s, I'm the %s script. Your ID is: %s" % (user_name, script, user_id)
print "I'd like to ask you a few questions."
print "Do you like me %s?" % user_name
likes = raw_input(prompt)

print "Where do you live %s?" % user_name
lives = raw_input(prompt)

print "What kind of computer do you have?"
computer = raw_input(prompt)

print """
Alright, so you said %r about liking me.
You live in %r. Not sure where that is.
And have a %r computer. Nice.
""" % (likes, lives, computer)

# Study Drill 1 -
# ----------------
# Zork: https://en.wikipedia.org/wiki/Zork
# It is a text based interactive game with finctional settings.
#
# Adeventure: https://en.wikipedia.org/wiki/Adventure_(1979_video_game)
# Its an Atari game, where you are a square avatar going through levels.
# It is also a graphical representation of -
# Colossal Cave Adventure-https://en.wikipedia.org/wiki/Colossal_Cave_Adventure
