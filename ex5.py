name = "Saif U. Azmi"
age = 20
height = 73  # inches
weight = 77  # kilograms
eyes = 'Brown'
teeth = 'White'
hair = 'Black'

print "Let's talk about %s." % name
print "He's %d inches tall." % height
print "He's %d kgs heavy." % weight
print "Actually that's not too heavy."
print "He's got %s eyes and %s hair." % (eyes, hair)
print "His teeth are usually %s depending on the coffee." % teeth

# this line is tricky, try to get it exactly right
print "If I add %d, %d and %d I get %d." % (
    age, height, weight, age + height + weight)

# Study Drill 2
height_in_centimeters = height * 2.54
weight_in_pounds = weight * 2.20

print "His height is {!r} in centimeters.".format(height_in_centimeters)
print "His weight is {0:.2f} in pounds.".format(round(weight_in_pounds, 2))

# Study Drill 3
# More about Python format characters: http://bit.ly/strFormatingOpr
# Note: the above mentioned is the old style of formating.
# It is advised to use the new formating style: http://bit.ly/newStrFormating
# The new style simply uses a function called 'format()' and variable like
# place holders.


# Interesting difference between %s and %r: http://bit.ly/strReprDiff

# TypeError: 'str' object is not callable
# This error occurs when you forget the % between the string and the list
# of variables.

# The float point problem and why using 'round(weight_in_pounds, 2)'
# doesn't give the desired result: http://bit.ly/floatpointLimitations
