# Stores a string with a format character and assigns the value to it
x = "There are %d types of people." % 10
# Stores a string
binary = "binary"
# Stores a string
do_not = "don't"
# Stores a string with two format cahracters and assigns value to both of them
y = "Those who know %s and those who %s." % (binary, do_not)
# SD2:              1                2

# Prints the value stored in variable 'x'
print x
# Prints the value stored in variable 'y'
print y

# Prints a string with a format character and assign value to it
print "I said: %r." % x
# SD2:         3
# Prints a string with a format character and assign value to it
print "I also said: '%s'." % y
# SD2:               4
# Stores a boolean value
hilarious = False
# Stores a string with a format charcter
joke_evaluation = "Isn't that joke so funny?! %r"

# Prints the value stored in 'joke_evaluation' variable and assigns value
# to its format character
print joke_evaluation % hilarious

# Stroes a string value
w = "This is the left side of..."
# Stores a string value
e = "a string with a right side."

# Prints the value stored variables 'w' and 'e'
print w + e

# Study Drill 3: there are only 4 places where a string is being put
# inside a string

# Study Drill 4: usingn '+' sign concatenates two strings hence making a
# longer string
