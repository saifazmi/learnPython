# A comment, this is so you can read your program later.
# Anything after the # is ignored by python.

print "I could have code like this."  # and the comment after is ignored.

# You can also use a comment to "disable" or comment out a piece of code:
# print "This won't run."

print "This will run."

# Study Drill 1
# I was right about what "#" does
# I call it the hash or pound symbol
# But "octothrope" seems like a very sophisticated word, better start using it.

# Study Drill 2
# Reviewed each line going backward, no errors found :)

# Study Drill 3
# Nope didn't find more mistakes

# Study Drill 4
# Reading backward definetly helps.
