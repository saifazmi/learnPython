# Maths symbols

# + plus
# - minus
# / slah
# * asterisk
# % percent
# < less-than
# > greater-than
# <= less-than-equal
# >= greater-than-equal

# Floating point numbers are fractions and have mre precision
# eg: 10.5, or 0.89, or even 3.0

# US uses PEMDAS which stands for Parentheses Exponents Multiplication
# Division Addition Subtraction.
# That is the order of operation followed by Python.
# This is different from BODMAS which stands for Brackets Orders
# (powers and roots) Division Multiplication Addition Subtraction.
# Most of the things being same the order of Division and Multiplication
# differs in these two formats.

# Prints a line of text
print "I will now count my chickens:"

# Prints 'Hens' and then calculates the number of hens
print "Hens", 25.0 + 30.0 / 6.0
# Prints 'Roosters' and then calculates the number of roorsters
print "Roosters", 100.0 - 25.0 * 3.0 % 4.0

# Prints a line of text
print "Now I will count the eggs:"

# Calculates the number of eggs and prints it out
print 3.0 + 2.0 + 1.0 - 5.0 + 4.0 % 2.0 - 1.0 / 4.0 + 6.0

# Prints a line of text
print "Is it true that 3 + 2 < 5 - 7?"

# Calculates if the above statement is true or not and prints the result
print 3.0 + 2.0 < 5.0 - 7.0

# Prints a text statement and then calculates and prints the result
print "What is 3 + 2?", 3.0 + 2.0
# Prints a text statement and then calculates nad prints the result
print "What is 5 - 7?", 5.0 - 7.0

# Prints a line of text
print "Oh, that's why it's False."

# Prints a line of text
print "How about some more."

# Prints a statement and then calculates and displays the result
print "Is it greater?", 5.0 > -2.0
# Prints a statement and then calculates and displays the result
print "Is it greater or equal?", 5.0 >= -2.0
# Prints a statement and then calculates and displays the result
print "Is it less or equal?", 5.0 <= -2.0

# Study Drill 2
# [8] <11:53> learnPython % python
# Python 2.7.6 (default, Jun 22 2015, 17:58:13)
# [GCC 4.8.2] on linux2
# Type "help", "copyright", "credits" or "license" for more information.
# >>> 2 + 3
# 5
# >>> 2 - 3
# -1
# >>> 2 / 3
# 0
# >>> 2.0 / 3.0
# 0.6666666666666666
# >>> 2 * 3
# 6
# >>> 2 % 3
# 2
# >>> 2.0 % 3.0
# 2.0
# >>> 2%
#   File "<stdin>", line 1
#     2%
#      ^
# SyntaxError: invalid syntax
# >>> 2 % 10
# 2
# >>> 10 % 2
# 0
# >>> 2 < 3
# True
# >>> 2 > 3
# False
# >>> 2 <= 3
# True
# >>> 2 >= 3
# False
