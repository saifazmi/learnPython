formatter = "%r %r %r %r"

print formatter % (1, 2, 3, 4)
print formatter % ("one", "two", "three", "four")
print formatter % (True, False, False, True)
print formatter % (formatter, formatter, formatter, formatter)
print formatter % (
    "I had this thing.",
    "That you could type up right.",
    "But it didn't sing.",
    "So I said goodnight."
)

# Study Drill 1: No errors :)

# Study Drill 2 -
# I think the last line uses both single and double quotes because
# we are using %r to output so it prints things as it is therefore
# the single quotes indicate that those are string objects
# and the reason why we have a double quote in one of them is
# because it has an apostrophe inside the string object so to
# make the difference clear it is wrapped in double quotes.
