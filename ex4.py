# Stores the number of available cars.
cars = 100
# Stores avilable space in each car.
space_in_a_car = 4.0
# Stores number of available drivers.
drivers = 30
# Stores number of passengers.
passengers = 90
# Stores number of cars not driven
cars_not_driven = cars - drivers
# Stores number of cars driven
cars_driven = drivers
# Stores the available carpool capacity
carpool_capacity = cars_driven * space_in_a_car
# Stores the number of average passengers in each car
average_passengers_per_car = passengers / cars_driven

print "There are", cars, "cars available."
print "There are only", drivers, "drivers available."
print "There will be", cars_not_driven, "empty cars today."
print "We can transport", carpool_capacity, "people today."
print "We have", passengers, "to carpool today."
print "We need to put about", average_passengers_per_car, "in each car."

# Traceback (most recent call last):
#   File "ex4.py", line 8, in <module>
#     average_passengers_per_car = car_pool_capacity / passenger
# NameError: name 'car_pool_capacity' is not defined

# Explaining the error:
# This error occured because no variable with the name 'car_pool_capacity'
# exists.

# Study Drill 1 & 6
# Changing 'space_in_a_car' to 4 from 4.0
#
# [30] <23:32> learnPython % python
# Python 2.7.6 (default, Jun 22 2015, 17:58:13)
# [GCC 4.8.2] on linux2
# Type "help", "copyright", "credits" or "license" for more information.
# >>> space_in_a_car = 4
# >>> drivers = 30
# >>> cars_driven = drivers
# >>> carpool_capacity = cars_driven * space_in_a_car
# >>> print carpool_capacity
# 120
#
# The overall calculation comes out as a whole number rather than floating
# point (fractions).

# Study Drill 4
# The equals sign '=' is use to assign values to the variables.
# The values assigned can be anything from words (strings) to numbers and
# others.
