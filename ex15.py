# imports the argv module
from sys import argv

# unpacks the command line arguments
script, filename = argv

# open the given file as a file object
txt = open(filename)

# A simple message and then read the contents of the file
print "Here's your file %r: " % filename
print txt.read()
# Close the file
txt.close()

# Ask for another file to read
print "Type the filename again: "
file_again = raw_input("> ")

# open the given file as a file object
txt_again = open(file_again)

# Read the contents of the file object
print txt_again.read()
# Close the file
txt_again.close()

# Study Drill 6 -
# ----------------
#
# Python 2.7.6 (default, Jun 22 2015, 17:58:13)
# [GCC 4.8.2] on linux2
# Type "help", "copyright", "credits" or "license" for more information.
# >>> txt = open(ex15_sample.txt)
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
# NameError: name 'ex15_sample' is not defined
# >>> txt = open("ex15_sample.txt")
# >>> txt.read()
# 'This is stuff I typed into a file.\nIt is really cool stuff.\nLots and lots
# of fun to have in here.'
